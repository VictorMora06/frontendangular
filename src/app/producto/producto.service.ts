import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from '../usuario/usuario.service';
import { Producto } from './interfaces/producto.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlBase:string="http://localhost:8087/api/productos";

  constructor(private http:HttpClient, private servicioUsuario:UsuarioService) { }

  httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  addAuthorizationHeader():any{
    let token = this.servicioUsuario.token;
    if(token != null){
      return this.httpHeaders.append('Authorization','Bearer '+token)
    }

    return this.httpHeaders;
  }


  //metodo para consumir api get
  mostrarProducto():Observable<Producto[]>{
    const url = this.urlBase
    return this.http.get<Producto[]>(url);
  }
  //metodo para enviar datos a api post
  guardarProducto(cliente:Producto):Observable<Producto>{
    const url = this.urlBase;
    return this.http.post<Producto>(url, cliente, {headers: this.addAuthorizationHeader() });
  }

  //buscar cliente por id
  getProducto(id:number):Observable<Producto>{
    return this.http.get<Producto>(`${this.urlBase}/${id}`,{headers: this.addAuthorizationHeader() });
  }

  //actualizar cliente
  update(cliente:Producto):Observable<Producto>{
    return this.http.put<Producto>(`${this.urlBase}/${cliente.id}`,cliente, {headers: this.addAuthorizationHeader() });
  }
  //eliminar cliente
  delete(id:number):Observable<Producto>{
    return this.http.delete<Producto>(`${this.urlBase}/${id}`,{headers: this.addAuthorizationHeader() });
  }
}
